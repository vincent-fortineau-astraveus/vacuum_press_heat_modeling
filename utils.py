import numpy as np
import re
import os
from scipy.signal import convolve

import control as ctrl
import pyswarms as ps

def lsq(na:int, nb:int, u:np.array, y:np.array, y_init:(np.array or None)):
    """
    The function uses the least squares method to estimate the parameters (AR and MA coefficients)
    of a linear time-invariant system described by the following equation:
    
        y[k] = a_1*u[k-1] + a_2*u[k-2] + ... + a_na*u[k-na] 
             + b_1*y[k-1] + b_2*y[k-2] + ... + b_nb*y[k-nb+1] + e[k]
    
    where:
    - y[k] is the output at time k,
    - u[k] is the input at time k,
    - a_1, a_2, ..., a_na are the AutoRegressive (AR) coefficients,
    - b_1, b_2, ..., b_nb are the eXternal model input (X) coefficients,
    - e[k] is the prediction error.

    The function builds a regression matrix 'theta' using past input and output data along with
    the specified initial conditions. The least squares solution is then used to estimate
    the AR and MA coefficients that minimize the sum of squared prediction errors.


    Args:
        na (int): Order of the autoregressive (AR, numerator) part of the system
        nb (int): Order of the external model input (X, denominator) part of the system
        u (np.array): Input signal
        y (np.array): Output signal
        y_init (np.array or None): Initial conditions for the output signal

    Returns:
        a_coeffs (numpy array): Estimated AR coefficients (numerator)
        b_coeffs (numpy array): Estimated X coefficients (denominator)
    """
    N = len(u)
    # building theta
    theta = np.zeros((N, na+nb-1))
    u_lag = np.zeros((na, 1))
    if y_init is None:
        y_lag = np.zeros((nb-1,1))
    else:
        y_lag = y_init.reshape((nb-1, 1))

    for k in range(N):
        u_lag = np.roll(u_lag, 1)
        u_lag[0] = u[k]
        y_lag = np.roll(y_lag, 1)
        if k > 0:
            y_lag[0] = y[k-1] 
        #
        for na_id in range(na):
            theta[k, na_id] = u_lag[na_id]
        for nb_id in range(nb-1):
            theta[k, na+nb_id] = -y_lag[nb_id]
    
    try:
        m = np.linalg.inv(theta.T @ theta) @ theta.T @ y
        mse = np.mean((y - theta @ m)**2)
    except:
        m, mse, _, _ = np.linalg.lstsq(theta, y, rcond=None)[0]

    return np.array(m[:na]), np.array(m[na:]), mse


def sim_arx(u:np.array, a:np.array, b:np.array, y_init:(np.array or None), snr_dB:(float or None)=None):
    """
    Simulate a system described by an ARX model.

        y[k] = a_1*u[k-1] + a_2*u[k-2] + ... + a_na*u[k-na] 
             + b_1*y[k-1] + b_2*y[k-2] + ... + b_nb*y[k-nb+1] + e[k]

    Args:
        u (np.array): Input signal
        a (np.array): AR coefficients
        b (np.array): X coefficients
        y_init (np.array or None): Initial conditions for the output signal
        snr_dB (float or None): Signal to Noise Ratio in dB, chose any positive value to complete the simulation with additive white Gaussian noise
    """
    N = len(u)
    u_lag = np.zeros((a.shape[0],1))
    if y_init is None:
        y_lag = np.zeros((b.shape[0],1))
    else:
        y_lag = y_init.reshape((b.shape[0], 1))
    y_sim = np.zeros(N)

    for k in range(N):
        u_lag = np.roll(u_lag,1)
        u_lag[0] = u[k]
        y_lag = np.roll(y_lag, 1)
        if k > 0:
            y_lag[0] = y_sim[k-1]
        y_sim[k] = a @ u_lag - b @ y_lag
    
    if snr_dB is not None:
            y_avg_power = np.mean(y_sim**2)
            y_avg_db = 10*np.log10(y_avg_power/10)
            noise_avg_power = 10**( (y_avg_db - snr_dB)/10 )
            noise = np.random.normal(0, np.sqrt(noise_avg_power), len(y_sim))
            y_sim = y_sim + noise

    return y_sim


def read_comsol_txt(file_path:str):
    """
    Read data from a COMSOL text file.

    Args:
        file_path (str): Path to the COMSOL text file.

    Returns:
        data (dict): A dictionary containing data from the COMSOL file.
                     Keys are headers, and values are corresponding arrays.
        headers (list): List of headers extracted from the first line of the file.
    """
    with open(file_path, 'r') as file:
        lines = file.readlines()
        data = {}
        # Extract headers from the first line (separated by irregular spaces)
        headers = re.split(r'\s+', lines[0].strip())
        # Initialize lists for each header in the dictionary
        for header in headers:
            data[header] = []
        # Iterate over the lines starting from the second line
        for line in lines[1:]:
            # Extract values using a regular expression to split by irregular spaces
            values = [float(value) for value in re.split(r'\s+', line.strip())]       
            # Append values to the corresponding header's list
            for header, value in zip(headers, values):
                data[header].append(value)
        # convert lists to numpy array
        for header in headers:
            data[header] = np.array(data[header])
    return data, headers


def read_pv3_heatdata_txt(file_path:str):
    """
    Read data from a vacuum press heat data text file.

    Args:
        file_path (str): Path to the vacuum press heat data text file.

    Returns:
        data (dict): A dictionary containing data from the vacuum press file.
                     Keys are headers, and values are corresponding arrays.
        headers (list): List of headers extracted from the first line of the file.
    """
    with open(file_path, 'r') as file:
        lines = file.readlines()
        data = {}
        # Extract headers from the first line (separated by irregular spaces)
        headers = re.split(r'\|\|\|', lines[0].strip())
        # Initialize lists for each header in the dictionary
        for header in headers:
            data[header] = []
        # Iterate over the lines starting from the second line
        for line in lines[1:]:
            # Extract values using a regular expression to split by irregular spaces
            try:
                values = [float(value) for value in re.split(r'\|\|\|', line.strip())]  
            except: 
                # The last line register might be incomplete and irrelevant, it is therefore ignored
                pass
            else:    
                # Append values to the corresponding header's list
                for header, value in zip(headers, values):
                    data[header].append(value)
        # convert lists to numpy array
        for header in headers:
            data[header] = np.array(data[header])
    return data, headers

def concatenate_pv3_heatdata(input_folder:str, output_file:str, heatdata_filename:(str or None)=None,separator='|||'):
    """
    Concatenate TXT files vertically using a specified separator and save the result to a new TXT file.
    The file are sorted with the following architecture:
    yy_mm_dd -|
              | - hh_mm_ss | 
              |            | - heatData | 
              |            |            | - name_1.txt
              |            |            | - name_2.txt
              |
              | - hh_mm_ss | 
              |            | - heatData | 
              |            |            | - name_1.txt
              | ...

    Args:
        input_folder (str): Path to the folder containing TXT files.
        output_file (str): Path to the output TXT file.
        heatdata_filename (str or None): Name of the target file, if None, all files are used. Default is None.
        separator (str): Separator used in the TXT files. Default is '|||'.

    Returns:
        None
    """
    # Get a list of all directories in the input folder
    subdirs = [d for d in os.listdir(input_folder) if os.path.isdir(os.path.join(input_folder, d))]
    subdirs.sort()
    # Open the output file in append mode
    with open(output_file, 'a') as outfile:
        # Iterate through each directory in the input folder
        for subdir in subdirs:
            subdir_path = os.path.join(input_folder, subdir)
            heatdata_path = os.path.join(subdir_path, 'heatData')
            # Check if 'heatData' directory exists
            if os.path.isdir(heatdata_path):
                heatdata_files = [f for f in os.listdir(heatdata_path) if f.endswith('.txt')]
                # Iterate through each TXT file in the 'heatData' directory
                for heatdata_file in heatdata_files:
                    heatdata_file_path = os.path.join(heatdata_path, heatdata_file)
                    # Skip files with a different name
                    if (heatdata_filename is not None) and (heatdata_file != heatdata_filename):
                        continue
                    # Open the current txt file for reading
                    with open(heatdata_file_path, 'r') as infile:
                        # Read the content of the TXT file
                        content = infile.read()
                        # Write the content to the output file
                        outfile.write(content)


def cost_function_DLQR(QR, state_space, max_desired_cmd):
    """
    Cost function for Particle Swarm Optimization (PSO) to tune DLQR controller.

    Args:
        QR (np.array): Concatenation of Q and R matrices for LQR tuning.
        state_space (StateSpace): Augmented state-space system with additional integral action.
        max_desired_cmd (float): Normalized peak value of the command relative to a unitary setpoint.

    Returns:
        J (list): List of cost values for each set of Q and R parameters.
    """
    J = []
    A = state_space.A
    B = state_space.B
    C = state_space.C
    D = state_space.D
    dt = state_space.dt
    # Extract Q and R matrices from the optimization variable QR
    Q_size = np.shape(A)[0]
    R_size = np.shape(B)
    Q_list = QR[:,:Q_size]
    R_list = QR[:,Q_size:]
    for Q_param, R_param in zip(Q_list, R_list):
        Q = np.diag(Q_param).squeeze()
        R = np.diag(R_param).squeeze()
        # Compute the feedback gain matrix K using DLQR
        K, _, _ = ctrl.dlqr(A, B, Q, R)
        # Full state feedback closed-loop output
        ss_cl = ctrl.StateSpace(A - B@K, [[0], [0], [dt]], C, D, dt)
        # Full state feedback closed-loop command
        ss_cl_u = ctrl.StateSpace(A - B@K, [[0], [0], [dt]], -K, D, dt) # this returns the command u
        # Computing the settling time and max cmd value
        try:
            settling_time = ctrl.step_info(ss_cl, SettlingTimeThreshold=0.05)['SettlingTime'] # this might fails if the response is too slow
        except:
            settling_time = 4000
        try:
            max_cmd_val = ctrl.step_info(ss_cl_u)['Peak']
        except:
            t = np.arange(0, 500, dt)
            _, cmd_vals = ctrl.step_response(ss_cl_u, t)
            max_cmd_val = max(cmd_vals)
        # Define the cost function components
        cost_command_response = max_cmd_val - max_desired_cmd
        if cost_command_response < 0:
            cost_command_response *= settling_time/2 # when the command is lower than the limit provided, the benefit is low
        else:
            cost_command_response *= 4000 # when the command is higher than the limit provided, the penalization is very strong
        J.append(settling_time + cost_command_response)
    return J

def tune_full_state_feedback_LQR(sys:(ctrl.TransferFunction or ctrl.StateSpace), u_lim:(float or None)=None, verbose:bool=True):
    """
    Tune the full state feedback control using Linear Quadratic Regulator (LQR) to achieve quick response,
    while optionally limiting the control signal.
    If a state space system is provided, then the second state should be the output.

    Args:
        sys (TransferFunction or StateSpace): Second-order system model.
        u_lim (float or None): Normalized peak value of the command relative to a unitary setpoint.
                              If None, no limitation is set on the control signal.
        verbose (bool): If True, display optimization progress; default is True.
    Returns:
        K (np.array): The tuned state feedback gain matrix.
        Q_opt (np.array): Optimized state weight matrix Q for LQR.
        R_opt (np.array): Optimized input weight matrix R for LQR.
    """
    # Check the system type
    if isinstance(sys, ctrl.TransferFunction):
        # Convert to State Space system
        a_coeff = np.array(sys.num).squeeze()
        b_coeff = np.array(sys.den).squeeze()
        A = np.array([[0,1], [-b_coeff[2],-b_coeff[1]]])
        B = np.array([[-a_coeff[1]/b_coeff[2]], [a_coeff[0]]])
        C = np.array([0, 1])
        D = np.array(0)
    elif isinstance(sys, ctrl.StateSpace):
        # No conversion required
        A = sys.A
        B = sys.B
        C = sys.C
        D = sys.D.squeeze()
    else:
        raise ValueError("Unsupported system type: {}".format(type(sys)))
    # Check if the system is discrete
    if not ctrl.isdtime(sys):
        raise ValueError("The system should be discrete.")
    else:
        dt = sys.dt
    # State space system augmentation for additional integral action
    A = np.vstack((np.hstack((A, [[0],[0]])), [0,-dt,1]))
    B = np.vstack((B,0))
    C = np.hstack((C,0))
    sys_aug = ctrl.ss(A,B,C,D,dt)
    # LQR tuning using Particle Swarm Optimization (PSO)
    dim = np.shape(A)[0] + np.shape(B)[1] # R is a scalar
    max_bound = 10000 * np.ones(dim) 
    min_bound = 0 * np.ones(dim)
    bounds = (min_bound, max_bound)
    options = {'c1': 0.5, 'c2': 0.3, 'w':0.9}
    kwargs={'state_space':sys_aug, 'max_desired_cmd': u_lim}
    optimizer = ps.single.GlobalBestPSO(n_particles=30, dimensions=dim, ftol = -1, options=options, bounds=bounds)
    cost, QR = optimizer.optimize(cost_function_DLQR, 100, n_processes=10, verbose=verbose,**kwargs)
    # Extract optimized state (Q) and input (R) weight matrices
    Q_opt = np.diag(QR[:np.shape(A)[0]]).squeeze()
    R_opt = np.diag(QR[np.shape(A)[0]:]).squeeze()
    # Calculate the optimal state feedback gain matrix K
    K_opt, S_opt, E_opt = ctrl.dlqr(A, B, Q_opt, R_opt)

    return K_opt, Q_opt, R_opt 

def simulate_full_state_feedback_closed_loop(sys:(ctrl.TransferFunction or ctrl.StateSpace), K:np.ndarray, input:(np.ndarray or float or None)=None, t_max:(float or None)=None, is_integral_action:bool=False):
    """
    Simulates the closed-loop response of a 2nd order system under full-state feedback control.
    If a state space system is provided, then the second state should be the output.

    Args:
        sys (ctrl.TransferFunction or ctrl.StateSpace): The system to be controlled, either in Transfer Function or State Space representation.
        input (np.ndarray or float): The input signal applied to the system.
        K (np.ndarray): The full-state feedback gain matrix. Should be a 1D array of dim 2 without the integral action, and 3 with the integral action.
        is_integral_action (bool, optional): Flag indicating whether integral action is added in the control. Defaults to False.
    Returns:
        tuple: A tuple containing time vector, closed-loop response, control command, open-loop response and the set input settling time.
    """
    # Check the system type
    if isinstance(sys, ctrl.TransferFunction):
        # Convert to State Space system
        a_coeff = np.array(sys.num).squeeze()
        b_coeff = np.array(sys.den).squeeze()
        A = np.array([[0,1], [-b_coeff[2],-b_coeff[1]]])
        B = np.array([[-a_coeff[1]/b_coeff[2]], [a_coeff[0]]])
        C = np.array([0, 1])
        D = np.array(0)
    elif isinstance(sys, ctrl.StateSpace):
        # No conversion required
        A = sys.A
        B = sys.B
        C = sys.C
        D = sys.D.squeeze()
    else:
        raise ValueError("Unsupported system type: {}".format(type(sys)))
    if not ctrl.isdtime(sys):
        raise ValueError("The system should be discrete. This function was not implemented for continuous systems.")
    else:
        dt = sys.dt
    if is_integral_action:
        # State space system augmentation for additional integral action
        A = np.vstack((np.hstack((A, [[0],[0]])), [0,-dt,1]))
        B = np.vstack((B,0))
        C = np.hstack((C,0))
    # open loop sys response
    ss_ol = ctrl.StateSpace(A, B, C, D, dt)
    # full state feedback closed loop response
    ss_cl = ctrl.StateSpace(A - B@K, [[0], [0], [dt]], C, D, dt)
    # full state feedback closed loop command
    ss_cl_u = ctrl.StateSpace(A - B@K, [[0], [0], [dt]], -K, D, dt) # this returns the command u

    # TODO: Check coherence between provided input and max time according to the sampling time, if not, reinterp input cmd.

    if t_max is None:
        # t1, _ = ctrl.step_response(ss_ol)
        # t2, _ = ctrl.step_response(ss_cl)
        # t3, _ = ctrl.step_response(ss_cl_u)
        # t_max = np.max(np.array([t1[-1],t2[-1],t3[-1]]))
        try:
            t_max = ctrl.step_info(ss_cl, SettlingTimeThreshold=0.01)['SettlingTime'] * 1.8
        except:
            t1, _ = ctrl.step_response(ss_ol)
            t2, _ = ctrl.step_response(ss_cl)
            t3, _ = ctrl.step_response(ss_cl_u)
            t_max = np.max(np.array([t1[-1],t2[-1],t3[-1]]))

    time_vector = np.arange(0, t_max, dt)
    
    if input is None:
        _, y_ol = ctrl.step_response(ss_ol, time_vector)
        _, y_cl = ctrl.step_response(ss_cl, time_vector)
        _, y_cl_u = ctrl.step_response(ss_cl_u, time_vector)
    else:
        _, y_ol = ctrl.forced_response(ss_ol, T=time_vector, U=input)
        _, y_cl = ctrl.forced_response(ss_cl, T=time_vector, U=input)
        _, y_cl_u = ctrl.forced_response(ss_cl_u, T=time_vector, U=input)
    
    try:
        settling_time = ctrl.step_info(ss_cl, SettlingTimeThreshold=0.01)['SettlingTime']
    except:
        settling_time = np.NaN

    return time_vector, y_cl, y_cl_u, y_ol, settling_time


def simulate_full_state_feedback_arx_closed_loop(sys:ctrl.TransferFunction, K:np.ndarray, target:(np.ndarray or float or None)=1, t_max:(float or None)=None, cmd_bound:(np.array or None)=np.array([-np.inf, np.inf]), y0:(float or None)=0, u0:(float or None)=0, is_integral_action:(bool or None)=False, is_windup:(bool or None)=False, deltaActivation:(float or None)=None):
    """
    Simulates the closed-loop response of a 2nd order system under full-state feedback control.
    If a state space system is provided, then the second state should be the output.
    ! Currently the function only accepts 2nd order systems in the form: 
    !    H(z) = (b1*z + b0)/(z^2 + a1*z + a0)

    Args:
        sys (ctrl.TransferFunction): The system to be controlled, in Transfer Function representation.
        K (np.ndarray): The full-state feedback gain matrix. Should be a 1D array of dim 2 without the integral action, and 3 with the integral action.
        target (np.ndarray or float or None, optional): The target value for the system. Defaults to 1.
        t_max (float or None, optional): The maximum time of the simulation. Defaults to None.
        cmd_bound (np.array or None, optional): The command bounds. Defaults to [-inf, inf].
        y0 (float or None, optional): The initial output value. Defaults to 0.
        u0 (float or None, optional): The initial input value. Defaults to 0.
        is_integral_action (bool or None, optional): Flag indicating whether integral action is added in the control. Defaults to False.
        is_windup (bool or None, optional): Flag indicating whether windup is added in the control. Defaults to False.
        TODO: implement deltaActivation
        deltaActivation (float or None, optional): The delta value for the FSF control to be activivated. Before its activation, the commands returns the max value.
                                                    Defaults to None. When set to None, the FSF control is always activated.
    Returns:
        tuple: A tuple containing time vector, closed-loop response, control command, open-loop response and the set input settling time.
    """
    # Check the system type
    if isinstance(sys, ctrl.TransferFunction):
        a_coeff = np.array(sys.num).squeeze()
        b_coeff = np.array(sys.den).squeeze()[1:] # the first coeff is 1 see equation below
        # (1*z^2 + a1*z + a0) y(z)  = (b1*z + b0) u(z)
        # y(k) + a1 y(k-1) + a0 y(k-2) = b1 u(k) + b0 u(k-1)
        # y(k) = b1 u(k) + b0 u(k-1) - a1 y(k-1) - a0 y(k-2)
    else:
        raise ValueError("Unsupported system type: {}".format(type(sys)))
    if not ctrl.isdtime(sys):
        raise ValueError("The system should be discrete. This function was not implemented for continuous systems.")
    else:
        dt = sys.dt
    
    if is_integral_action:
        x = np.zeros((3,1))
    else:
        x = np.zeros((2,1))

    y = y0*np.ones((2,1)) # initial condition (temperature), y[1] = y(k-1), y[0] = y(k)
    u = u0*np.ones((2,1)) # initial condition (command signal)

    y_record = []
    u_record = []

    t_last = 0
    u_max = cmd_bound[1]
    u_min = cmd_bound[0]

    is_u_sat = False

    t_sim = np.arange(0, t_max, dt)

    for t in t_sim:
        # compute the state of the system without the integral action
        x[0] = y[1] - y0 - a_coeff[1]/b_coeff[1] * (u[1] - u0)
        x[1] = y[0] - y0
        # compute the integral state of the system
        if is_integral_action:
            x_int = x[2] + (t - t_last)*(target - y[0])
        if is_windup and is_u_sat:
            pass # do not change x[2]
        else:
            x[2] = x_int
        # compute the command signal
        u[0] = -K@x
        is_u_sat = False
        if u[0] > u_max:
            # x[2] = x[2] - Kt*(u[0] - u_max)*(t - t_last) # anti windup
            u[0] = u_max
            is_u_sat = True
        elif u[0] < u_min:
            # x[2] = x[2] - Kt*(u[0] - u_min)*(t - t_last) # anti windup
            u[0] = u_min
        y_record.append(y[0])
        u_record.append(u[0])
        # compute the temperature using the ARX model
        y_out = a_coeff@u - b_coeff@y 
        u = np.roll(u, 1)
        y = np.roll(y, 1)
        y[0] = y_out
        t_last = t

    return t_sim, np.array(y_record), np.array(u_record)

def simulate_pid_arx_closed_loop(sys:ctrl.TransferFunction, PID:np.ndarray, target:(np.ndarray or float or None)=1, t_max:(float or None)=None, cmd_bound:(np.ndarray or None)=np.array([-np.inf, np.inf]), y0:(float or None)=0, u0:(float or None)=0, is_windup:(bool or None)=False):
    """
    Simulates the closed-loop response of a system under PID control.

    Args:
        sys (ctrl.TransferFunction): The system to be controlled, in Transfer Function representation.
        PID (np.ndarray): PID gains, in the order, proportional, integral, derivative. Should be a 1D array of dim 3.
        target (np.ndarray or float or None, optional): The target value for the system. Defaults to 1.
        t_max (float or None, optional): The maximum time of the simulation. Defaults to None.
        cmd_bound (np.ndarray or None, optional): The command bounds. Defaults to [-inf, inf].
        y0 (float or None, optional): The initial output value. Defaults to 0.
        u0 (float or None, optional): The initial input value. Defaults to 0.
        is_windup (bool or None, optional): Flag indicating whether windup is added in the control. Defaults to False.
    Returns:
        tuple: A tuple containing time vector, closed-loop response, control command, open-loop response and the set input settling time.
    """
    # Check the system type
    if isinstance(sys, ctrl.TransferFunction):
        a_coeff = np.array(sys.num).squeeze()
        b_coeff = np.array(sys.den).squeeze()[1:] # the first coeff is 1 see equation below
        # (1*z^2 + a1*z + a0) y(z)  = (b1*z + b0) u(z)
        # y(k) + a1 y(k-1) + a0 y(k-2) = b1 u(k) + b0 u(k-1)
        # y(k) = b1 u(k) + b0 u(k-1) - a1 y(k-1) - a0 y(k-2)
    else:
        raise ValueError("Unsupported system type: {}".format(type(sys)))
    if not ctrl.isdtime(sys):
        raise ValueError("The system should be discrete. This function was not implemented for continuous systems.")
    else:
        dt = sys.dt
    t_sim = np.arange(0, t_max, dt)

    y = y0*np.ones((len(a_coeff),1)) # initial condition (temperature), y[1] = y(k-1), y[0] = y(k)
    u = u0*np.ones((len(b_coeff),1)) # initial condition (command signal)
    y_record = np.zeros(t_sim.shape)
    u_record = np.zeros(t_sim.shape)
    
    e_sum = 0
    e_last = 0
    u_max = cmd_bound[1]
    u_min = cmd_bound[0]
    is_u_sat = False

    Kp = PID[0]
    Ki = PID[1]
    Kd = PID[2]

    for k in range(len(t_sim)):
        e = target - y[0] # error
        if is_windup and is_u_sat:
            pass # if windup, do not change e_sum
        else:
            e_sum = e_sum + e*(dt)
        u[0] = Kp*e + Ki*e_sum + Kd*(e - e_last)/dt
        is_u_sat = False
        if u[0] > u_max:
            u[0] = u_max
            is_u_sat = True
        elif u[0] < u_min:
            u[0] = u_min
        y_record[k] = y[0].squeeze()
        u_record[k] = u[0].squeeze()
        # compute the new y using the ARX model
        y_out = a_coeff@u - b_coeff@y 
        u = np.roll(u, 1)
        y = np.roll(y, 1)
        y[0] = y_out

    return t_sim, y_record, u_record

def simulate_arx_cascade_closed_loop(sys_out:ctrl.TransferFunction, sys_in:ctrl.TransferFunction, K:np.ndarray, PID:np.ndarray, target:(np.ndarray or float or None)=1, t_max:(float or None)=None, cmd_bound_out:(np.array or None)=np.array([-np.inf, np.inf]), cmd_bound_in:(np.array or None)=np.array([-np.inf, np.inf]), y0:(float or None)=0, u0:(float or None)=0, w0:(float or None)=0, is_integral_action:(bool or None)=False, is_windup:(bool or None)=False):
    """
    Simulates the closed-loop response of a 2nd order system under full-state feedback control.
    If a state space system is provided, then the second state should be the output.
    ! Currently the function only accepts 2nd order systems in the form: 
    !    H(z) = (b1*z + b0)/(z^2 + a1*z + a0)

    Args:
        sys1 (ctrl.TransferFunction): The first system to be controlled (outter), in Transfer Function representation.   
        sys2 (ctrl.TransferFunction): The second system to be controlled (inner), in Transfer Function representation.
        K (np.ndarray): The cascade full state feedback gain matrix. Should be a 1D array of dim 2 without the integral action, and 3 with the integral action.
        PID (np.ndarray): The PID controller gain matrix. Should be a 1D array of dim.
        target (np.ndarray or float or None, optional): The target value for the system. Defaults to 1.
        t_max (float or None, optional): The maximum time of the simulation. Defaults to None.
        cmd_bound (np.array or None, optional): The command bounds. Defaults to [-inf, inf].
        y0 (float or None, optional): The initial output value. Defaults to 0.
        u0 (float or None, optional): The initial input value. Defaults to 0.
        w0 (float or None, optional): The initial inner system output value. Defaults to 0.
        is_integral_action (bool or None, optional): Flag indicating whether integral action is added in the control. Defaults to False.
        is_windup (bool or None, optional): Flag indicating whether windup is added in the control. Defaults to False.
    Returns:
        tuple: A tuple containing time vector, closed-loop response, control command, open-loop response and the set input settling time.
    """
    # Check the system type
    if isinstance(sys_out, ctrl.TransferFunction):
        a_coeff_out = np.array(sys_out.num).squeeze()
        b_coeff_out = np.array(sys_out.den).squeeze()[1:] # the first coeff is 1 see equation below
        # (1*z^2 + a1*z + a0) y(z)  = (b1*z + b0) u(z)
        # y(k) + a1 y(k-1) + a0 y(k-2) = b1 u(k) + b0 u(k-1)
        # y(k) = b1 u(k) + b0 u(k-1) - a1 y(k-1) - a0 y(k-2)
    else:
        raise ValueError("Unsupported system type: {}".format(type(sys_out)))
    if not ctrl.isdtime(sys_out):
        raise ValueError("The system should be discrete. This function was not implemented for continuous systems.")
    else:
        dt_out = sys_out.dt
    if isinstance(sys_in, ctrl.TransferFunction):
        a_coeff_in = np.array(sys_in.num).squeeze()
        b_coeff_in = np.array(sys_in.den).squeeze()[1:] # the first coeff is 1 see equation below
        # (1*z^2 + a1*z + a0) y(z)  = (b1*z + b0) u(z)
        # y(k) + a1 y(k-1) + a0 y(k-2) = b1 u(k) + b0 u(k-1)
        # y(k) = b1 u(k) + b0 u(k-1) - a1 y(k-1) - a0 y(k-2)
    else:
        raise ValueError("Unsupported system type: {}".format(type(sys_in)))
    if not ctrl.isdtime(sys_in):
        raise ValueError("The system should be discrete. This function was not implemented for continuous systems.")
    else:
        dt_in = sys_in.dt
    
    if is_integral_action:
        x = np.zeros((3,1))
    else:
        x = np.zeros((2,1))

    y = y0*np.ones((2,1)) # initial condition (outter system output), y[1] = y(k-1), y[0] = y(k)
    u = u0*np.ones((2,1)) # initial condition (outter system command signal)
    v = u0*np.ones((2,1)) # initial condition (inner system command signal)
    w = w0*np.ones((2,1)) # initial condition (inner system output signal)

    y_record = []
    u_record = []
    w_record = []
    v_record = []

    t_last = 0
    u_max = cmd_bound_out[1]
    u_min = cmd_bound_out[0]
    v_max = cmd_bound_in[1]
    v_min = cmd_bound_in[0]

    is_u_sat = False
    is_w_sat = False

    t_sim_out = np.arange(0, t_max, dt_out)
    t_sim_in = np.arange(0, t_max, dt_in)
    e_sum = 0
    e_last = 0

    for t in t_sim_out:
        # compute the state of the system without the integral action
        x[0] = y[1] - y0 - a_coeff_out[1]/b_coeff_out[1] * (u[1] - u0)
        x[1] = y[0] - y0
        # compute the integral state of the system
        if is_integral_action:
            if is_windup and is_u_sat:
                pass # do not change x[2]
            else:
                x[2] = x[2] + (t - t_last)*(target - y[0])
        # compute full state system contol
        u[0] = -K@x
        is_u_sat = False
        if u[0] > u_max:
            # x[2] = x[2] - Kt*(u[0] - u_max)*(t - t_last) # anti windup
            u[0] = u_max
            is_u_sat = True
        elif u[0] < u_min:
            # x[2] = x[2] - Kt*(u[0] - u_min)*(t - t_last) # anti windup
            u[0] = u_min
        # The PID target is the outter system output
        for _ in range(int(dt_out/dt_in)):
            # compute PID control
            e = u[0] - w[0] # error
            if is_windup and is_w_sat:
                pass # if windup, do not change e_sum
            else:
                e_sum = e_sum + e*(dt_in)
            v[0] = PID[0]*e + PID[1]*e_sum + PID[2]*(e - e_last)/dt_in
            e_last = e
            is_u_sat = False
            if v[0] > v_max:
                v[0] = v_max
                is_u_sat = True
            elif v[0] < v_min:
                v[0] = v_min
            # compute the inner system output
            w_out = a_coeff_in@v - b_coeff_in@w + y[0]
            v = np.roll(v, 1)
            w = np.roll(w, 1)
            w[0] = w_out
        y_out = a_coeff_out@w - b_coeff_out@y
        u = np.roll(u, 1)
        y = np.roll(y, 1)
        y[0] = y_out

    return t_sim_out, np.array(y_record), np.array(u_record), t_sim_in, np.array(w_record), np.array(v_record)

def simulate_arx_cascade_open_loop(sys_out:ctrl.TransferFunction, sys_in:ctrl.TransferFunction, t_in:float, u_in:np.ndarray, y0:(float or None)=0, w0:(float or None)=0):
    """
    Simulates the closed-loop response of a 2nd order system under full-state feedback control.
    If a state space system is provided, then the second state should be the output.
    ! Currently the function only accepts 2nd order systems in the form: 
    !    H(z) = (b1*z + b0)/(z^2 + a1*z + a0)

    Args:
        sys1 (ctrl.TransferFunction): The first system to be controlled (outter), in Transfer Function representation.   
        sys2 (ctrl.TransferFunction): The second system to be controlled (inner), in Transfer Function representation.
        t_max (float or None, optional): The maximum time of the simulation. Defaults to None.
        y0 (float or None, optional): The initial output value. Defaults to 0.
        u0 (float or None, optional): The initial input value. Defaults to 0.
        w0 (float or None, optional): The initial inner system output value. Defaults to 0.
    Returns:
        tuple: A tuple containing time vector, closed-loop response, control command, open-loop response and the set input settling time.
    """
    # Check the system type
    if isinstance(sys_out, ctrl.TransferFunction):
        a_coeff_out = np.array(sys_out.num).squeeze()
        b_coeff_out = np.array(sys_out.den).squeeze()[1:] # the first coeff is 1 see equation below
        # (1*z^2 + a1*z + a0) y(z)  = (b1*z + b0) u(z)
        # y(k) + a1 y(k-1) + a0 y(k-2) = b1 u(k) + b0 u(k-1)
        # y(k) = b1 u(k) + b0 u(k-1) - a1 y(k-1) - a0 y(k-2)
    else:
        raise ValueError("Unsupported system type: {}".format(type(sys_out)))
    if not ctrl.isdtime(sys_out):
        raise ValueError("The system should be discrete. This function was not implemented for continuous systems.")
    else:
        dt_out = sys_out.dt
    if isinstance(sys_in, ctrl.TransferFunction):
        a_coeff_in = np.array(sys_in.num).squeeze()
        b_coeff_in = np.array(sys_in.den).squeeze()[1:]
    else:
        raise ValueError("Unsupported system type: {}".format(type(sys_in)))
    if not ctrl.isdtime(sys_in):
        raise ValueError("The system should be discrete. This function was not implemented for continuous systems.")
    else:
        dt_in = sys_in.dt

    y = y0*np.ones((2,1)) # initial condition (outter system output), y[1] = y(k-1), y[0] = y(k)
    w = w0*np.ones((2,1)) # initial condition (inner system output signal)

    y_record = []
    w_record = []

    t_sim_out = np.arange(0, t_in[-1], dt_out)
    t_sim_in = np.arange(0, t_in[-1], dt_in)
    u_in = np.interp(t_sim_out, t_in, u_in)
    u = u_in[0]*np.ones((2,1)) # initial condition (input signal)

    for new_u in u_in:
        u[0] = new_u
        # inner system
        for _ in range(int(dt_out/dt_in)):
            w_out = a_coeff_in@u - b_coeff_in@w + y[0]
            w_record.append(w[0])
            w = np.roll(w, 1)
            w[0] = w_out
        y_out = a_coeff_out@w - b_coeff_out@y
        y_record.append(y[0])
        y = np.roll(y, 1)
        y[0] = y_out

    return t_sim_out, np.array(y_record), t_sim_in, np.array(w_record)

def get_steady_state(t:np.array, y:np.array, threshold:(float or None)=0.05):
    """
    Returns the system steady state time according to threshold.

    Args:
        t (np.array): Time vector (should be of shape (N,1) or (N,)).
        y (np.array): Output data vector (should be of shape (N,1) or (N,)).
        target (float or None, optional): Target value. Defaults to None.

    Returns:
        float: Returns steady state time. If the system does not reach a steady state, the function returns -1.
    """
    # TODO: This function might returns false positives...
    dy = np.diff(y, axis=0)
    try:
        t_stable = t[np.where(np.abs(dy) >= threshold)[0][-1]]
    except:
        return -1
    if t_stable == t[-1]:
        return -1
    else:
        return t_stable
    

def get_settling_time(t:np.array, y:np.array, settling_percent:(float or None)=0.05, target:(float or None)=None):
    """
    Calculates the settling time of the system based on the final value of the output signal.
    If the system does not reach a steady time, the function returns -1.

    Args:
        t (np.array): Time vector (should be of shape (N,1) or (N,)).
        y (np.array): Output data vector (should be of shape (N,1) or (N,)).
        target (float): Target value.
        settling_percent (float or None, optional): Settling percentage. Defaults to 0.05.

    Returns:
        float: Settling time. If the system does not reach a steady state, the function returns -1.
    """
    t_steady = get_steady_state(t, y)
    if t_steady < 0:
        return -1
    
    if target is None:
        final_value = np.mean(y[np.where(t >= t_steady)[0]])
    else:
        final_value = target

    err = final_value - y
    t_err_last_thresh = t[np.where(np.abs(err) >= 0.05*final_value)[0][-1]]
    return t_err_last_thresh

def get_first_peak_time(time, y):
    # indices of local maxima
    peaks_indices = (y[:-2] < y[1:-1]) & (y[1:-1] > y[2:])
    first_peak_index = np.argmax(peaks_indices)
    first_peak_time = time[first_peak_index + 1]  # Add 1 to account for slicing offset   
    return first_peak_time

def interpolate_with_latch(new_time, time, data):
    """
    Interpolates data with latch to the last measured data point.

    Args:
        time (array-like): Original time vector.
        data (array-like): Original data vector.
        new_time (array-like): New time vector for interpolation.

    Returns:
        array-like: Interpolated data with latch.
    """
    interpolated_data = np.zeros_like(new_time)  # Initialize interpolated data vector

    last_index = 0  # Initialize index of last measured data point

    # Iterate through each new time point
    for i, t in enumerate(new_time):
        # Find the index of the last measured data point
        while last_index < len(time) - 1 and time[last_index + 1] <= t:
            last_index += 1
        
        # Set the interpolated data point to the last measured data point
        interpolated_data[i] = data[last_index]

    return interpolated_data

def moving_average(a, n=3, axis=-1):
    def _moving_average(a_1d, n):
        ret = np.cumsum(a_1d, dtype=float)
        ret[n:] = ret[n:] - ret[:-n]
        return ret[n - 1:] / n

    if axis is None or a.ndim == 1:
        # Apply moving average to the flattened array or 1D array
        result = _moving_average(a, n)

        # Calculate padding size
        padding_size = (n - 1) % len(a)

        # Pad the result with the first value
        first_value = a[0]
        padding = np.full(padding_size, first_value, dtype=result.dtype)
        result = np.insert(result, 0, padding)

        return result
    else:
        # Apply moving average along the specified axis
        result = np.apply_along_axis(_moving_average, axis, a, n)

        # Calculate padding size
        padding_size = (n - 1) % a.shape[axis]

        # Pad the result with the first value along the specified axis
        first_value = a[tuple([slice(None)] * axis + [0] + [slice(None)] * (a.ndim - axis - 1))]
        padding = np.full((padding_size,) + result.shape[:axis] + result.shape[axis + 1:], first_value, dtype=result.dtype)
        result = np.insert(result, 0, padding, axis=axis)

        return result

def identify_PV3_stack_2nd_order_model(file_path:str, dt:float, na:(int or None)=2, nb:(int or None)=3, min_steady_state_time:(float or None)=None):
    """
    Identifies the PV3 stack model for each step target using an ARX least square method.

    Args:
        file_path (str): Path to the PV3 heat data file.
        dt (float): Sampling time.
        na (int or None, optional): AR order (numerator of transfer function). Defaults to 2.
        nb (int or None, optional): X order (denominator of transfer funciton). Defaults to 3.
        min_steady_state_time (float or None, optional): Minimum steady state time to be used for identification. Defaults to None.

    Returns:
        tuple: Tuple containing:
            - a_coeff_list (numpy array): AR coefficients list for each identification
            - b_coeff_list (numpy array): X coefficients for each identification
            - mse_list (float): Mean squared error for each identification
    """
    data, headers = read_pv3_heatdata_txt(file_path)

    t = (data['Time'] - data['Time'][0]) / 1e3 # conversion to seconds 
    d_temp = np.array(data['Target']) # desired temperature
    out_temp = np.array(data['Thermocouple']) # output temperature measured with thermocouple
    cart_names = ["CT_" + str(i//4+1) + str((i%4) + 1) for i in range(16)]
    ct_temp = []
    for cart_name in cart_names:
        ct_temp.append(data[cart_name])
    ct_temp = np.array(ct_temp)
    # only the 4 middle cartridge temperatures are used for identification
    ct_middle_group = [6,7,10,11]
    ct_avg_middle_temp = np.mean(ct_temp[ct_middle_group], axis=0)
    # interpolation over the desired sampling rate
    t_n = np.arange(0, t[-1], dt)
    d_temp_n = np.interp(t_n, t, d_temp)
    out_temp_n = np.interp(t_n, t, out_temp)
    ct_avg_middle_temp_n = np.interp(t_n, t, ct_avg_middle_temp)
    # detect rising and falling edges of the setpoint for identification
    rising_edges = [i for i, val in enumerate(np.diff(np.sign(d_temp_n))) if val > 0]
    falling_edges = [i-1 for i, val in enumerate(np.diff(np.sign(d_temp_n))) if val < 0]
    # deleting the edges that are too close to each other
    if min_steady_state_time is not None:
        unfinished_heating_phase = []
        for i, edge_idx in enumerate(rising_edges):
            if falling_edges[i] - edge_idx < 500/dt:
                unfinished_heating_phase.append(i)
        rising_edges = [val for idx, val in enumerate(rising_edges) if idx not in unfinished_heating_phase]
        falling_edges = [val for idx, val in enumerate(falling_edges) if idx not in unfinished_heating_phase]
    # least square identification for each rising edge
    a_coeff_list = []
    b_coeff_list = []
    mse_list = []
    for t0, t1 in zip(rising_edges, falling_edges):
        u0 = ct_avg_middle_temp_n[t0]
        y0 = out_temp_n[t0]
        a_coeff, b_coeff, mse = lsq(na, nb, u=ct_avg_middle_temp_n[t0:t1]-u0, y=out_temp_n[t0:t1]-y0, y_init=np.zeros((nb-1,1)))
        a_coeff_list.append(a_coeff)
        b_coeff_list.append(b_coeff)
        mse_list.append(mse)

    return a_coeff_list, b_coeff_list, mse_list